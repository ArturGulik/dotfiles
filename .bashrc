#
# ~/.bashrc
#

# Interactive shell settings only
if [[ $- == *i* ]]; then
  # No interactive shell settings for now
  false
fi

colors() {
  local fgc bgc vals seq0

  printf "Color escapes are %s\n" '\e[${value};...;${value}m'
  printf "Values 30..37 are \e[33mforeground colors\e[m\n"
  printf "Values 40..47 are \e[43mbackground colors\e[m\n"
  printf "Value  1 gives a  \e[1mbold-faced look\e[m\n\n"

  # foreground colors
  for fgc in {30..37}; do
    # background colors
    for bgc in {40..47}; do
      fgc=${fgc#37} # white
      bgc=${bgc#40} # black

      vals="${fgc:+$fgc;}${bgc}"
      vals=${vals%%;}

      seq0="${vals:+\e[${vals}m}"
      printf "  %-9s" "${seq0:-(default)}"
      printf " ${seq0}TEXT\e[m"
      printf " \e[${vals:+${vals+$vals;}}1mBOLD\e[m"
    done
    echo
    echo
  done
}

[ -r /usr/share/bash-completion/bash_completion ] && . /usr/share/bash-completion/bash_completion

# Change the window title of X terminals
case ${TERM} in
xterm* | rxvt* | Eterm* | aterm | kterm | gnome* | interix | konsole*)
  PROMPT_COMMAND='echo -ne "\033]0;${USER}@${HOSTNAME%%.*}:${PWD/#$HOME/\~}\007"'
  ;;
screen*)
  PROMPT_COMMAND='echo -ne "\033_${USER}@${HOSTNAME%%.*}:${PWD/#$HOME/\~}\033\\"'
  ;;
esac

use_color=true

# Set colorful PS1 only on colorful terminals.
# dircolors --print-database uses its own built-in database
# instead of using /etc/DIR_COLORS.  Try to use the external file
# first to take advantage of user additions.  Use internal bash
# globbing instead of external grep binary.
safe_term=${TERM//[^[:alnum:]]/?} # sanitize TERM
match_lhs=""
[[ -f ~/.dir_colors ]] && match_lhs="${match_lhs}$(<~/.dir_colors)"
[[ -f /etc/DIR_COLORS ]] && match_lhs="${match_lhs}$(</etc/DIR_COLORS)"
[[ -z ${match_lhs} ]] &&
  type -P dircolors >/dev/null &&
  match_lhs=$(dircolors --print-database)
[[ $'\n'${match_lhs} == *$'\n'"TERM "${safe_term}* ]] && use_color=true

if ${use_color}; then
  # Enable colors for ls, etc.  Prefer ~/.dir_colors #64489
  if type -P dircolors >/dev/null; then
    if [[ -f ~/.dir_colors ]]; then
      eval $(dircolors -b ~/.dir_colors)
    elif [[ -f /etc/DIR_COLORS ]]; then
      eval $(dircolors -b /etc/DIR_COLORS)
    fi
  fi

  if [[ ${EUID} == 0 ]]; then
    PS1='\[\033[01;31m\][\h\[\033[01;36m\] \W\[\033[01;31m\]]\$\[\033[00m\] '
  else
    PS1='\[\033[01;32m\][\u@\h\[\033[01;37m\] \W\[\033[01;32m\]]\$\[\033[00m\] '
  fi

  alias ls='ls --color=auto'
  alias grep='grep --colour=auto'
  alias egrep='egrep --colour=auto'
  alias fgrep='fgrep --colour=auto'
else
  if [[ ${EUID} == 0 ]]; then
    # show root@ when we don't have colors
    PS1='\u@\h \W \$ '
  else
    PS1='\u@\h \w \$ '
  fi
fi

unset use_color safe_term match_lhs sh

alias cp="cp -i"     # confirm before overwriting something
alias df='df -h'     # human-readable sizes
alias free='free -m' # show sizes in MB
alias np='nano -w PKGBUILD'
alias more=less
alias hdmi-copy-screen='xrandr --output eDP --same-as HDMI-A-0 && xrandr --output HDMI-A-0 --mode 1920x1080 --rate 144.00'
alias extendhdmi='xrandr --output HDMI-A-0 --mode 1920x1080 && xrandr --output eDP --left-of HDMI-A-0 && xrandr --output HDMI-A-0 --mode 1920x1080 --rate 144.00'

xhost +local:root >/dev/null 2>&1

# Bash won't get SIGWINCH if another process is in the foreground.
# Enable checkwinsize so that bash will check the terminal size when
# it regains control.  #65623
# http://cnswww.cns.cwru.edu/~chet/bash/FAQ (E11)
shopt -s checkwinsize

shopt -s expand_aliases

# export QT_SELECT=4

# Enable history appending instead of overwriting.  #139609
shopt -s histappend

#
# # ex - archive extractor
# # usage: ex <file>
ex() {
  if [ -f $1 ]; then
    case $1 in
    *.tar.bz2) tar xjf $1 ;;
    *.tar.gz) tar xzf $1 ;;
    *.bz2) bunzip2 $1 ;;
    *.rar) unrar x $1 ;;
    *.gz) gunzip $1 ;;
    *.tar) tar xf $1 ;;
    *.tbz2) tar xjf $1 ;;
    *.tgz) tar xzf $1 ;;
    *.zip) unzip $1 ;;
    *.Z) uncompress $1 ;;
    *.7z) 7z x $1 ;;
    *) echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}
[ -r /home/ag/.byobu/prompt ] && . /home/ag/.byobu/prompt #byobu-prompt#

#My variable to help wxwidgets programs find needed libraries
# export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib

#My variable to set grep color to yellow
export GREP_COLORS='ms=01;33:mc=01;31:sl=:cx=:fn=35:ln=32:bn=32:se=36'

#My personal aliases
alias screenfetch="screenfetch -w"
alias icat="kitty +kitten icat"
alias powerthefuckoff=poweroff
alias powerthefuckingfuckoff=poweroff
alias wifi=nmtui
alias servephp="php -S localhost:8000"

removesvgclips() {
  cat $1 | perl -ne "s/clip-path=\".*\"//g; print;"
}

cmdranking() {
  history | awk 'BEGIN {FS="[ \t]+|\\|"} {print $3}' | sort | uniq -c | sort -nr | head -10
}
tocsv() {
  ssconvert -O 'separator=;' --export-type=Gnumeric_stf:stf_assistant $1 data.csv
}
hump() {
  args="$*"
  echo $args
  perl -MPOSIX -E "say "
}
alias pshell="perl -MPOSIX -nWE \"print '> '; say eval $1\""
ddg() {
  query="$*"
  query=$(echo "$query" | tr ' ' '+')
  firefox "https://duckduckgo.com/?t=ffab&q=$query" &
}

alias duck="ddg"

sumrows() {
  filepath=$1
  awk '{print $0 "+"} END{print "0"}' $filepath | node -p
}
here() {
  files="$(file $(find . -type f | grep -vE '~|.svg$|.pdf$|.png$|.jpg$|.git|.log') |
    awk -F: '/ASCII text|Unicode text/ {print $1}')"
  if [[ $1 != "" ]]; then
    grep $2 -n -H $1 $files
    return
  fi
  echo $files
}
here2() {
  files="$(find . -type f -regextype posix-extended ! -regex '^.*(svg|pdf|png|jpg|xcf|~|#|zip|xlsx|pptx|wpress|log)$')"
  if [[ $1 != "" ]]; then
    grep $2 -n -H $1 $files
    return
  fi
  echo $files
}
addclass() {
  if [[ $1 == "" ]]; then
    echo "Provide the name of the new class"
    return
  fi
  upper=$(echo $1 | tr [:lower:] [:upper:])_H

  if [[ -f "$1.h" ]]; then
    echo "$1.h already exists."
    return
  fi
  if [[ -f "$1.cpp" ]]; then
    echo "$1.cpp already exists."
    return
  fi

  touch $1.h

  echo "#ifndef $upper" >$1.h
  echo "#define $upper" >>$1.h
  echo "" >>$1.h
  echo "#include <iostream>" >>$1.h
  echo "" >>$1.h
  echo "class $1;" >>$1.h
  echo "" >>$1.h
  echo "class $1 {" >>$1.h
  echo "private:" >>$1.h
  echo "public:" >>$1.h
  echo "  $1();" >>$1.h
  echo "  ~$1();" >>$1.h
  echo "};" >>$1.h
  echo "" >>$1.h
  echo "#endif" >>$1.h

  touch $1.cpp

  echo "#include \"$1.h\"" >>$1.cpp
  echo "" >>$1.cpp
  echo "$1::$1() {" >>$1.cpp
  echo "" >>$1.cpp
  echo "}" >>$1.cpp
  echo "" >>$1.cpp
  echo "$1::~$1() {" >>$1.cpp
  echo "" >>$1.cpp
  echo "}" >>$1.cpp
}
better_git_status() {
  f=0
  s="$(git status -s 2>/dev/null)"
  b="$(git branch --list 2>/dev/null)"
  if [[ $b != "" ]]; then
    git branch --list
    if [[ $s != "" ]]; then
      echo "───────────────────"
      git status -s
    fi
    f=1
  fi

  if [[ $f == 0 ]]; then
    echo This is not a git repository!
  fi
  return
}
git() {
  if [ $# -eq 0 ]; then
    better_git_status
    return
  fi
  if [ "$1" == "god" ]; then
    shift
    git branch -r | grep -v '\->' | sed "s,\x1B\[[0-9;]*[a-zA-Z],,g" | while read remote; do git branch --track "${remote#origin/}" "$remote"; done
    git fetch --all
    git pull --all

    return
  fi
  if [ "$1" == "new" ]; then
    shift
    if [ $# -eq 0 ]; then
      echo You have to provide a name for the new branch!
      return
    fi
    git checkout -b $1
    return
  fi
  if [ "$1" == "dotfiles" ]; then
    shift
    if [ $# -eq 0 ]; then
      command git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME status
      return
    fi
    if [ "$1" == "backup" ]; then
      shift
      mkdir -p .dotfiles.bak
      git dotfiles checkout 2>&1 | grep -E "\s+\." | awk {'print $1'} | xargs -I{} mv {} .dotfiles.bak/{}
      return
    fi
    command git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME "$@"
    return
  fi
  if [ "$1" == "stats" ]; then
    shift
    git log --shortstat --no-merges --pretty="%cE" | sed 's/\(.*\)@.*/\1/' | grep -v "^$" | awk 'BEGIN { line=""; } !/^ / { if (line=="" || !match(line, $0)) {line = $0 "," line }} /^ / { print line " # " $0; line=""}' | sort | sed -E 's/# //;s/ files? changed,//;s/([0-9]+) ([0-9]+ deletion)/\1 0 insertions\(+\), \2/;s/\(\+\)$/\(\+\), 0 deletions\(-\)/;s/insertions?\(\+\), //;s/ deletions?\(-\)//' | awk 'BEGIN {name=""; files=0; insertions=0; deletions=0;} {if ($1 != name && name != "") { print name ": " files " files changed, " insertions " insertions(+), " deletions " deletions(-), " insertions-deletions " net"; files=0; insertions=0; deletions=0; name=$1; } name=$1; files+=$2; insertions+=$3; deletions+=$4} END {print name ": " files " files changed, " insertions " insertions(+), " deletions " deletions(-), " insertions-deletions " net";}'
    return
  fi
  command git "$@"
}

# Move the newest downloaded file to pwd
download() {
  filename="$(\ls ~/Downloads --sort time | head -1)"
  mv -i ~/Downloads/$filename $PWD
}
#Make copying easier
alias xclip="xclip -selection \"clipboard\""
alias paste="xclip -selection \"clipboard\" -o"
#My program aliases
alias bin=". bin"
alias pro=". pro"
alias doc=". doc"
alias pic=". pic"
alias dld=". dld"
alias back=". back"

alias cat="bat"
#Dangerous exa directory hiding functionality:
hidden="Desktop|Music|Videos|Templates|Public|Pictures|Documents|Downloads|Projects|*~"
exa_command="exa --ignore-glob=\"$hidden\""

#Autocd
shopt -s autocd

alias tarball="tar cvzf"

#My ls options
ls_options="--group-directories-first"
ls_command="$exa_command $ls_options"

alias {l,k,a,s,d,ls,sl,lsl,sls,la,ld,ka,ks,kd}="$ls_command"
alias ll="$ls_command -l"

alias unixlab="ssh inf152061@unixlab.cs.put.poznan.pl"
alias brightness="xrandr --output eDP --brightness"
alias checknet="ping gnu.org"
alias brightness2="sudo nano /sys/class/backlight/amdgpu_bl0/brightness"
alias screensaver="xset dpms force off"

#aliases for emacs
alias emacs-server="\emacs --daemon"
#alias emacs="emacsclient -c -a 'echo \"Launch the server dumbass!\"'"
#alias emacs-cli="emacsclient -a 'echo \"Launch the server dumbass!\"'"

alias modelsim="/home/ag/intelFPGA/20.1/modelsim_ase/linux/vsim"

alias vsim="/home/ag/intelFPGA/20.1/modelsim_ase/linux/vsim"

alias menu="firefox /home/ag/praca/setka/output/MenuWroclaw.pdf"
alias menu-text="firefox /home/ag/praca/setka/output/MenuWroclawText.pdf"
alias ludziki="inkscape /home/ag/praca/setka/000_elementy_000/ludziki/ludziki.svg"

alias amek="make"
alias amke="make"
alias emak="make"
alias mak="make"
alias maek="make"

random() { shuf -i "$1" -n 1; }
alias greek="sed -n \`random 1-24\`p ~/galphabet"

alias fixkeyboard="xmodmap ~/.Xmodmap"
# eval "$(starship init bash)"

# BEGIN_KITTY_SHELL_INTEGRATION
if test -n "$KITTY_INSTALLATION_DIR" -a -e "$KITTY_INSTALLATION_DIR/shell-integration/bash/kitty.bash"; then source "$KITTY_INSTALLATION_DIR/shell-integration/bash/kitty.bash"; fi
# END_KITTY_SHELL_INTEGRATION
. "$HOME/.cargo/env"

alias fixsudo="systemctl start systemd-homed"
alias useless-pkgs="pacman -Qtdq"

alias fetchDiscordVersion="curl --silent 'https://discord.com/api/download?platform=linux&format=tar.gz' | perl -0777 -ne 'print \"$&\n\" if /(?<=apps\/linux\/).*?(?=\/)/'"

alias android-studio="~/android-studio/bin/studio.sh"

alias metadata="exiftool"

function _comp_commands() {
  local cur=$2

  COMPREPLY=($(~/.local/bin/ag-complete.sh $cur))
}

complete -o bashdefault -I -F _comp_commands

function pdfsize() {
  pdfinfo "$@" | grep "Page size" | grep -Eo '[-+]?[0-9]*\.?[0-9]+' | awk -v x=0.35277778 -v ORS=' x ' '{print $1*x "mm"} END {ORS=a; print "\n"}' | sed 's/...$//'
}

function pdftopng() {
  pdftoppm "$@" outputname -png
}

# Returns the path to a binary the given alias leads to,
# or returns the argument unchanged if it was not an alias
function getcommand() {
  if type -t "$@" | grep --silent alias; then
    type -a "$@" | head -1 | perl -ne "s/.*\`//; s/'//g; print;"
    return
  fi
  echo "$@"
}

alias matrix="tmatrix"

function qrcodegenerateag() {
  if [ $# -eq 0 ]; then
    echo "Syntax: qrcodegenerateag \"https://example.org\""
    return
  fi
  qrencode -d 1 -l M --strict-version -v 5 --size 1 -m 0 -t svg -o qrCode.svg --foreground 000000 --background 00000000 "$@"
  echo "Generated qrCode.svg in pwd"
}

export PATH="/home/ag/.local/share/gem/ruby/3.0.0/bin:$PATH"
